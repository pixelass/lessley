# Lessley
[![Build Status](https://travis-ci.org/pixelass/lessley.svg)](https://travis-ci.org/pixelass/lessley)
[![devDependency Status](https://david-dm.org/pixelass/lessley/dev-status.svg)](https://david-dm.org/pixelass/lessley#info=devDependencies)

## A testing suite for Less.js written in Less..

*like Whaaa??**


## Urban meaning of Lessley:

> A Lessley is the most kind, caring and loveable person you will meet. the lessley will be there for you through any thing, never leave your side, and always be there for you when you need a shoulder to cry on...

**In other words... Lessley's got your back**

## Test
```less
.describe("The 'toBe' matcher compares with =",{
  .it("and has a positive case", {
    .expect(true);.toBe(true);
  });

  .it("and can have a negative case", {
    .expect(false);.not.toBe(true);
  });
});
```

## Result
```css
 /**
  * @test The 'toBe' matcher compares with =
  * @result {
  *   ✔︎: and has a positive case;
  *   ✔︎: and can have a negative case;
}
/**/
```

## grunt-lessley

You can use `grunt lessley` from this package: https://github.com/pixelass/grunt-lessley

### Screenshot
![test](test/log-tests.png?0001)
