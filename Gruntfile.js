/* global module*/
module.exports = function (grunt) {
  'use strict';
  require('time-grunt')(grunt);

  grunt.initConfig({
    less: {
      test: {
        options: {
          strictMath: true
        },
        files: {
          'test/test.css': 'test/test.less'
        }
      }
    },
    notify: {
      less: {
        options: {
          title: 'Success',
          message: 'All .less files have been compiled'
        }
      },
      csslint: {
        options: {
          title: 'Success',
          message: 'csslint happiness'
        }
      }
    },
    csslint: {
      strict: {
        options: {},
        src: 'test/test.css'
      }
    },
    lessley: {
      test: {
        src: 'test/test.css'
      }
    },
    watch: {
      styles: {
        files: ['**/*.less'],
        tasks: ['less:test', 'notify:less', 'lessley:test'],
        options: {
          nospawn: true
        }
      },
      lint: {
        files: ['**/*.less'],
        tasks: ['csslint:strict', 'notify:csslint'],
        options: {
          nospawn: true
        }
      }
    },
    attention: {
      test: {
        options: {
          message: 'Test started',
          border: 'thin',
          borderColor: 'green'
        }
      }
    }
  });



  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-lessley');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-attention');

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('test', [
    'less:test',
    'attention:test',
    'csslint:strict',
    'lessley:test']);
};